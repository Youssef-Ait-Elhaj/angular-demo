import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReusableComponent } from './components/reusable/reusable.component';



@NgModule({
  declarations: [
    ReusableComponent
  ],
  exports: [
    ReusableComponent
  ],
  imports: [
    CommonModule
  ]
})
export class SharedModule { }
